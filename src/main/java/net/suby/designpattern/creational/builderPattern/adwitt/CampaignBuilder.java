package net.suby.designpattern.creational.builderPattern.adwitt;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class CampaignBuilder {
    private String name;
    private String spendCap;
    private String promotedObject;
    private String dailyBudget;
    private String lifetimeBudget;
}
