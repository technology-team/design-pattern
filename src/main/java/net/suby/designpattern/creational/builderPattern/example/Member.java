package net.suby.designpattern.creational.builderPattern.example;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Member {
    private String name;
    private int age;
    private String phone;
    private String address;


    @Builder
    @ToString
    public static class BuilderMember {
        private String name;
        private int age;
        private String phone;
        @Builder.Default
        private String address = "경기도 광명시";
    }
}
