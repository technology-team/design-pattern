package net.suby.designpattern.creational.builderPattern.example;

public class Execute {

    public static void main(String[] args) {
        // 호출 코드만으로는 각 인자의 의미를 알기 어렵다.
        Member member1 = new Member("myborn", 37, "010-1234-5678", "경기도 광명시");
        System.out.println(member1.toString());

        // 객체를 생성한 뒤 값을 setting 한다.
        // 여러번의 메소드 호출로 나누어져 인스턴스를 생성되므로 생성과정을 거치는 동안 객체가 일관된 상태를 유지하지 못할 수 있다.
        // 값이 완료된 후 다음 프로세스 진행시 정말 모든 값이 set된것인지 판단어려움
        Member member2 = new Member();
        member2.setName("myborn2");
        member2.setAge(37);
        System.out.println(member2.toString());

        // 값을 setting 한뒤 한번에 객체를 생성한다.
        MemberBuilder member3 = new MemberBuilder.Builder("myborn3", 37)
                .address("경기도 광명시")
                .build();
        System.out.println(member3);


        Member.BuilderMember member4 = Member.BuilderMember.builder()
                .name("myborn4")
                .age(37)
                .build();

        System.out.println(member4.toString());
    }
}
