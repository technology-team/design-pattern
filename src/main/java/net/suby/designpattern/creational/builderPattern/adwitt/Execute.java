package net.suby.designpattern.creational.builderPattern.adwitt;

import java.util.HashMap;
import java.util.Map;

public class Execute {

    public static void main(String[] args) {
        Map<String, String> request = new HashMap<>();
        request.put("name", "campaign name");
        request.put("dailyBudget", "5000");
        request.put("promotedObject", "{promotedObject}");

        CampaignBuilder campaignBuilder = CampaignBuilder.builder()
                .name(request.get("name"))
                .dailyBudget(request.get("dailyBudget"))
                .promotedObject(request.get("promotedObject"))
                .build();

        System.out.println(campaignBuilder);
    }
}
