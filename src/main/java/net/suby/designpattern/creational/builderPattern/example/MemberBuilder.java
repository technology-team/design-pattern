package net.suby.designpattern.creational.builderPattern.example;

import lombok.ToString;

@ToString
public class MemberBuilder {
    private String name;
    private int age;
    private String phone;
    private String address;


    private MemberBuilder(Builder builder) {
        this.name = builder.name;
        this.age = builder.age;
        this.phone = builder.phone;
        this.address = builder.address;
    }


    public static class Builder {
        private String name;
        private int age;
        private String phone;
        private String address;

        public Builder(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public MemberBuilder build() {
            return new MemberBuilder(this);
        }
    }
}
