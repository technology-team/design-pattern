package net.suby.designpattern.creational.factoryMethodPattern.adwitt;

import java.math.BigDecimal;

public class DailyBudget extends Budget {
    private final String dailyBudget;
    private final Integer currency;

    public DailyBudget(Request request) {
        this.dailyBudget = request.getDailyBudget();
        this.currency = request.getCurrency();
    }

    @Override
    public String getBudget() {
        return new BigDecimal(Double.parseDouble(this.dailyBudget) * this.currency).toPlainString();
    }
}
