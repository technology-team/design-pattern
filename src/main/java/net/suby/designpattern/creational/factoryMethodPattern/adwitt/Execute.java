package net.suby.designpattern.creational.factoryMethodPattern.adwitt;

public class Execute {

    public static void main(String[] args) {
        Request request = new Request();
        request.setBudgetType(BudgetType.DAILY_BUDGET);
        request.setCurrency(10);
        request.setDailyBudget("5000");
        //request.setLifetimeBudget("3000");

        /*
        if (request.getDailyBudget() != null) {
            params.put("daily_budget",(new BigDecimal(Double.parseDouble(request.getDailyBudget()) * request.getCurrency()).toPlainString()));
        }
        if (request.getLifetimeBudget() != null) {
            params.put("lifetime_budget",(new BigDecimal(Double.parseDouble(request.getLifetimeBudget()) * request.getCurrency()).toPlainString()));
        }
         */


        Budget budget = Budget.budgetFactory(request);
        System.out.println(budget.getBudget());


        //        request.setBudgetType(BudgetType.LIFETIME_BUDGET);
        //        Budget lifetimeBudget = Budget.budgetFactory(request);
        //        System.out.println(lifetimeBudget.getBudget());
    }
}
