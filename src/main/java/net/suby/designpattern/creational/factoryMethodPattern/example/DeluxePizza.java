package net.suby.designpattern.creational.factoryMethodPattern.example;

public class DeluxePizza extends Pizza {

    private int price = 13000;

    @Override
    public int GetPrice() {
        return this.price;
    }
}
