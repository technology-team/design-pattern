package net.suby.designpattern.creational.factoryMethodPattern.adwitt;

import lombok.Data;

@Data
public class Request {
    private BudgetType budgetType;
    private Integer currency;
    private String lifetimeBudget;
    private String dailyBudget;
}
