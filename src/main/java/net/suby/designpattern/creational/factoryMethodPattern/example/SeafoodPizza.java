package net.suby.designpattern.creational.factoryMethodPattern.example;

public class SeafoodPizza extends Pizza {

    private int price = 17000;

    @Override
    public int GetPrice() {
        return this.price;
    }
}
