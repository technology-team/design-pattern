package net.suby.designpattern.creational.factoryMethodPattern.adwitt;

public enum BudgetType {
    DAILY_BUDGET, LIFETIME_BUDGET
}
