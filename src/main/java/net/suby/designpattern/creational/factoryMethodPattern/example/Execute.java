package net.suby.designpattern.creational.factoryMethodPattern.example;

public class Execute {

    public static void main(String[] args) {
        Pizza seafoodPizza = Pizza.PizzaFactory(Pizza.PizzaType.Seafood);
        System.out.println(seafoodPizza.GetPrice());


        Pizza hamMushroomPizza = Pizza.PizzaFactory(Pizza.PizzaType.HamMushroom);
        System.out.println(hamMushroomPizza.GetPrice());

    }
}
