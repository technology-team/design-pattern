package net.suby.designpattern.creational.factoryMethodPattern.example;

public abstract class Pizza {
    public static Pizza PizzaFactory(PizzaType pizzaType) {
        System.out.println(pizzaType.toString());
        switch (pizzaType) {
            case HamMushroom:
                return new HamAndMushroomPizza();
            case Deluxe:
                return new DeluxePizza();
            case Seafood:
                return new SeafoodPizza();
        }
        return null;
    }

    public abstract int GetPrice();

    public enum PizzaType {
        HamMushroom, Deluxe, Seafood
    }
}
