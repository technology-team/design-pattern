package net.suby.designpattern.creational.factoryMethodPattern.adwitt;

public abstract class Budget {
    public static Budget budgetFactory(Request request) {
        switch (request.getBudgetType()) {
            case DAILY_BUDGET:
                return new DailyBudget(request);
            case LIFETIME_BUDGET:
                return new LifeTimeBudget(request);
        }
        return null;
    }

    public abstract String getBudget();
}
