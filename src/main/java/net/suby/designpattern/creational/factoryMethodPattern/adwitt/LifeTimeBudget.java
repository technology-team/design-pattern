package net.suby.designpattern.creational.factoryMethodPattern.adwitt;

import java.math.BigDecimal;

public class LifeTimeBudget extends Budget {
    private final String lifetimeBudget;
    private final Integer currency;

    public LifeTimeBudget(Request request) {
        this.lifetimeBudget = request.getLifetimeBudget();
        this.currency = request.getCurrency();
    }

    @Override
    public String getBudget() {
        return new BigDecimal(Double.parseDouble(this.lifetimeBudget) * this.currency).toPlainString();
    }
}
