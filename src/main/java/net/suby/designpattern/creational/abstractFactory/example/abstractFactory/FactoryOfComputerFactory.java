package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public class FactoryOfComputerFactory {
    public void createComputer(String type) {
        ComputerFactory computerFactory = null;
        switch (type) {
            case "LG":
                computerFactory = new LGComputerFactory();
                break;
            case "Samsung":
                computerFactory = new SamsungComputerFactory();
                break;
        }

        computerFactory.createKeyboard();
        computerFactory.createMouse();
        // 추가 공통 로직 구성
    }
}
