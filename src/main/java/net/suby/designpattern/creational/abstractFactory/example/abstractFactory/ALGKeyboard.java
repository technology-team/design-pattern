package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public class ALGKeyboard implements AKeyboard {
    public ALGKeyboard() {
        System.out.println("LG 키보드 생성");
    }
}
