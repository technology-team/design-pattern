package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public class ALGMouse implements AMouse {

    public ALGMouse() {
        System.out.println("LG Mouse 생성");
    }
}
