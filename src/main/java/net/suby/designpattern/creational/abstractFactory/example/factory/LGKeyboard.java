package net.suby.designpattern.creational.abstractFactory.example.factory;

public class LGKeyboard implements Keyboard {
    public LGKeyboard() {
        System.out.println("LG 키보드 생성");
    }
}
