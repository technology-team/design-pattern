package net.suby.designpattern.creational.abstractFactory.adwitt;

public class GoogleInsightFactory implements PlatformInsightFactory {
    @Override
    public CampaignInsight createCampaignInsight() {
        return new GoogleCampaignInsight();
    }

    @Override
    public AdsetInsight createAdsetInsight() {
        return new GoogleAdsetInsight();
    }
}
