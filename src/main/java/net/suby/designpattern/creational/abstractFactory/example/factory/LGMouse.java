package net.suby.designpattern.creational.abstractFactory.example.factory;

public class LGMouse implements Mouse {

    public LGMouse() {
        System.out.println("LG Mouse 생성");
    }
}
