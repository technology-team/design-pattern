package net.suby.designpattern.creational.abstractFactory.example.factory;

public class SamsungKeyboard implements Keyboard {
    public SamsungKeyboard() {
        System.out.println("Samsung 키보드 생성");
    }
}
