package net.suby.designpattern.creational.abstractFactory.example.factory;

public class KeyboardFactory {

    public Keyboard createKeyboard(String type) {
        Keyboard keyboard = null;

        switch (type) {
            case "LG":
                keyboard = new LGKeyboard();
                break;
            case "Samsung":
                keyboard = new SamsungKeyboard();
                break;
        }
        return keyboard;
    }
}
