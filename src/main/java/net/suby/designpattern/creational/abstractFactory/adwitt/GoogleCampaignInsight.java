package net.suby.designpattern.creational.abstractFactory.adwitt;

public class GoogleCampaignInsight implements CampaignInsight {

    public GoogleCampaignInsight() {
        System.out.println("Google Campaign Insights");
    }
}
