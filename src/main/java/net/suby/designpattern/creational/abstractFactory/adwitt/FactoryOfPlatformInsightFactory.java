package net.suby.designpattern.creational.abstractFactory.adwitt;

public class FactoryOfPlatformInsightFactory {
    public void getInsight(String platform) {
        PlatformInsightFactory platformInsightFactory = null;

        switch (platform) {
            case "GOOGLE":
                platformInsightFactory = new GoogleInsightFactory();
                break;
            case "FACEBOOK":
                platformInsightFactory = new FacebookInsightFactory();
                break;
        }

        platformInsightFactory.createCampaignInsight();
        platformInsightFactory.createAdsetInsight();
    }
}
