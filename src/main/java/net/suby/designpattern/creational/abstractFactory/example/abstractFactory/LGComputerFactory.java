package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public class LGComputerFactory implements ComputerFactory {

    public ALGKeyboard createKeyboard() {
        return new ALGKeyboard();
    }

    public ALGMouse createMouse() {
        return new ALGMouse();
    }
}
