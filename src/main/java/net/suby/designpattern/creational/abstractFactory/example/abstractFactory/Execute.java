package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public class Execute {
    public static void main(String[] args) {
        FactoryOfComputerFactory factoryOfComputerFactory = new FactoryOfComputerFactory();
        factoryOfComputerFactory.createComputer("Samsung");
    }
}
