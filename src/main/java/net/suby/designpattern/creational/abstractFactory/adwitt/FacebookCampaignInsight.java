package net.suby.designpattern.creational.abstractFactory.adwitt;

public class FacebookCampaignInsight implements CampaignInsight {

    public FacebookCampaignInsight() {
        System.out.println("Facebook Campaign Insights");
    }
}
