package net.suby.designpattern.creational.abstractFactory.example.factory;

public class Execute {

    public static void main(String[] args) {
        ComputerFactory computerFactory = new ComputerFactory();
        computerFactory.createComputer("LG");
    }
}
