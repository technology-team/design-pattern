package net.suby.designpattern.creational.abstractFactory.adwitt;

public class Execute {
    public static void main(String[] args) {
        FactoryOfPlatformInsightFactory factoryOfPlatformInsightFactory = new FactoryOfPlatformInsightFactory();
        factoryOfPlatformInsightFactory.getInsight("FACEBOOK");
        factoryOfPlatformInsightFactory.getInsight("GOOGLE");
    }
}
