package net.suby.designpattern.creational.abstractFactory.adwitt;

public class FacebookInsightFactory implements PlatformInsightFactory {
    @Override
    public CampaignInsight createCampaignInsight() {
        return new FacebookCampaignInsight();
    }

    @Override
    public AdsetInsight createAdsetInsight() {
        return new FacebookAdsetInsight();
    }
}
