package net.suby.designpattern.creational.abstractFactory.adwitt;

public class FacebookAdsetInsight implements AdsetInsight {

    public FacebookAdsetInsight() {
        System.out.println("Facebook Adset Insights");
    }
}
