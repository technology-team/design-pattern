package net.suby.designpattern.creational.abstractFactory.example.factory;

public class SamsungMouse implements Mouse {

    public SamsungMouse() {
        System.out.println("Samsung mouse 생성");
    }
}
