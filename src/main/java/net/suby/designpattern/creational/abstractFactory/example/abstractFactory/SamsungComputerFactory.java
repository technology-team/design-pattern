package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public class SamsungComputerFactory implements ComputerFactory {

    public ASamsungKeyboard createKeyboard() {
        return new ASamsungKeyboard();
    }

    public ASamsungMouse createMouse() {
        return new ASamsungMouse();
    }
}
