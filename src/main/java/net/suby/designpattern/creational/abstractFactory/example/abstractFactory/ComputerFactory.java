package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public interface ComputerFactory {
    public AKeyboard createKeyboard();

    public AMouse createMouse();
}
