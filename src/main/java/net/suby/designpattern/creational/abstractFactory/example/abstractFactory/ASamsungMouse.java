package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public class ASamsungMouse implements AMouse {

    public ASamsungMouse() {
        System.out.println("Samsung mouse 생성");
    }
}
