package net.suby.designpattern.creational.abstractFactory.adwitt;

public interface PlatformInsightFactory {
    public CampaignInsight createCampaignInsight();

    public AdsetInsight createAdsetInsight();
}
