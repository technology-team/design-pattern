package net.suby.designpattern.creational.abstractFactory.example.factory;

public class ComputerFactory {

    public void createComputer(String type) {
        KeyboardFactory keyboardFactory = new KeyboardFactory();
        MouseFactory mouseFactory = new MouseFactory();

        keyboardFactory.createKeyboard(type);
        mouseFactory.createMouse(type);

        // 본체, 모니터, 스피커 등등 기본 구성품이 더 많아지게 된다면 더 많은 메소드 팩토리를 만들어야 한다.
        System.out.println("---- " + type + "컴퓨터완성");
    }
}
