package net.suby.designpattern.creational.abstractFactory.example.abstractFactory;

public class ASamsungKeyboard implements AKeyboard {
    public ASamsungKeyboard() {
        System.out.println("Samsung 키보드 생성");
    }
}
