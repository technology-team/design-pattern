package net.suby.designpattern.creational.prototypePattern.adwitt;

import java.util.ArrayList;
import java.util.List;

public class Execute {

    public static void main(String[] args) throws CloneNotSupportedException {
        List<ReportSpec> reportSpecs = new ArrayList<>();

        ReportSpec reportSpec = new ReportSpecImpl();
        reportSpec.setColumnName("id,name");
        for (int i = 1; i < 7; i++) {
            ReportSpec cloneReportSpec = reportSpec.clone();
            cloneReportSpec.setStartDate("2019-01-0" + i);
            cloneReportSpec.setEndDate("2019-01-0" + i);
            reportSpecs.add(cloneReportSpec);
        }

        System.out.println(reportSpecs);
    }

    /*
     switch (rrs.getTimeIncrement()) {
                case "1":
                    for (; st <= et; st = c.getTimeInMillis()) {
                        String time = sdf.format(c.getTime());
                        Map<String, Object> cpd = cloner.deepClone(adObjectMap);
                        for (Object o : cpd.values()) {
                            Map<String, Object> adItem = (Map<String, Object>) o;
                            adItem.put("start_time", time);
                            adItem.put("end_time", time);
                        }
                        segmentalizedMap.put(time, cpd);
                        c.add(Calendar.DAY_OF_MONTH, 1);
                    }
                    break;
                case "7":
                    for (; st <= et; st = c.getTimeInMillis()) {
                        String startTime = sdf.format(c.getTime());
                        c.add(Calendar.DAY_OF_MONTH, 6);
                        if (c.getTimeInMillis() > et) {
                            c.setTimeInMillis(et);
                        }
                        String endTime = sdf.format(c.getTime());
                        Map<String, Object> cpd = cloner.deepClone(adObjectMap);
                        for (Object o : cpd.values()) {
                            Map<String, Object> adItem = (Map<String, Object>) o;
                            adItem.put("start_time", startTime);
                            adItem.put("end_time", endTime);
                        }
                        c.add(Calendar.DAY_OF_MONTH, 1);
                        segmentalizedMap.put(startTime, cpd);
                    }
                    break;
     */
}
