package net.suby.designpattern.creational.prototypePattern.adwitt;

import lombok.ToString;

@ToString
public class ReportSpecImpl extends ReportSpec {
    private String startDate;
    private String endDate;
    private String columnName;

    @Override
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @Override
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
}
