package net.suby.designpattern.creational.prototypePattern.example;

public abstract class Prototype implements Cloneable {

    public Prototype clone() throws CloneNotSupportedException {
        return (Prototype) super.clone();
    }

    public abstract void printX();

    public abstract int getX();

    public abstract void setX(int x);
}
