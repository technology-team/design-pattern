package net.suby.designpattern.creational.prototypePattern.adwitt;

public abstract class ReportSpec implements Cloneable {

    public ReportSpec clone() throws CloneNotSupportedException {
        return (ReportSpec) super.clone();
    }

    public abstract void setStartDate(String startDate);

    public abstract void setEndDate(String endDate);

    public abstract void setColumnName(String columnName);
}
