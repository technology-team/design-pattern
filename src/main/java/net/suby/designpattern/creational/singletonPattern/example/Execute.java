package net.suby.designpattern.creational.singletonPattern.example;

public class Execute {

    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getSingletonObject();
        System.out.println(singleton1);
        Singleton singleton2 = Singleton.getSingletonObject();
        System.out.println(singleton2);
    }
}
