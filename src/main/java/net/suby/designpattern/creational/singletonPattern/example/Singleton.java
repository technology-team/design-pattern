package net.suby.designpattern.creational.singletonPattern.example;

public class Singleton {
    private Singleton() {}

    public static Singleton getSingletonObject() {
        return SingletonHolder.INSTANCE;
    }


    private static class SingletonHolder {
        public static final Singleton INSTANCE = new Singleton();
    }
}
