package net.suby.designpattern.behavioral.strategy.adwitt;

import net.suby.designpattern.behavioral.strategy.adwitt.strategies.PaymentMethod;

public class Charge {
    private int amount = 0;

    public void setPaymentMothod(PaymentMethod paymentMethod) {
        paymentMethod.pay(this.amount);
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
