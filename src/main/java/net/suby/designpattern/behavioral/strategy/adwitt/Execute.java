package net.suby.designpattern.behavioral.strategy.adwitt;

import net.suby.designpattern.behavioral.strategy.adwitt.strategies.Card;
import net.suby.designpattern.behavioral.strategy.adwitt.strategies.VirtualAccount;

public class Execute {
    public static void main(String[] args) {
        Charge charge = new Charge();
        charge.setAmount(5000);
        charge.setPaymentMothod(new Card());
        charge.setPaymentMothod(new VirtualAccount());
    }
}
