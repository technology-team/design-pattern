package net.suby.designpattern.behavioral.strategy.example2.strategies;

public interface AttackStrategy {
    public void attack();
}
