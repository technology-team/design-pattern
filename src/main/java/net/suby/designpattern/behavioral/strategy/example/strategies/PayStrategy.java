package net.suby.designpattern.behavioral.strategy.example.strategies;

public interface PayStrategy {
    boolean pay(int paymentAmount);

    void collectPaymentDetails();
}
