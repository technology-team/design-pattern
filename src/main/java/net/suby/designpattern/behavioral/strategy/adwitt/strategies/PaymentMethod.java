package net.suby.designpattern.behavioral.strategy.adwitt.strategies;

public interface PaymentMethod {
    public void pay(int paymentAmount);
}
