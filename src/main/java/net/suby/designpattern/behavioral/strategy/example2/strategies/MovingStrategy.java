package net.suby.designpattern.behavioral.strategy.example2.strategies;

public interface MovingStrategy {
    public void move();
}
