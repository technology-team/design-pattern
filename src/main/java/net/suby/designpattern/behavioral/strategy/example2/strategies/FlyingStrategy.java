package net.suby.designpattern.behavioral.strategy.example2.strategies;

public class FlyingStrategy implements MovingStrategy {
    @Override
    public void move() {
        System.out.println("I can fly");
    }
}
