package net.suby.designpattern.behavioral.strategy.adwitt.strategies;

public class VirtualAccount implements PaymentMethod {
    @Override
    public void pay(int paymentAmount) {
        System.out.println("가상계좌로 " + paymentAmount + "을 계산합니다.");
    }
}
