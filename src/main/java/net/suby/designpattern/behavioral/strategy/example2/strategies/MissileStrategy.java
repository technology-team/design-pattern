package net.suby.designpattern.behavioral.strategy.example2.strategies;

public class MissileStrategy implements AttackStrategy {
    @Override
    public void attack() {
        System.out.println("I have Missile");
    }
}
