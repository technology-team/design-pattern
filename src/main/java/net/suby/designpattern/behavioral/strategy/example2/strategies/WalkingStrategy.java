package net.suby.designpattern.behavioral.strategy.example2.strategies;

public class WalkingStrategy implements MovingStrategy {
    @Override
    public void move() {
        System.out.println("I can only walk");
    }
}
