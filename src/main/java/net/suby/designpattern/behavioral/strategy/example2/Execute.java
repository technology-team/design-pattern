package net.suby.designpattern.behavioral.strategy.example2;

import net.suby.designpattern.behavioral.strategy.example2.robot.Atom;
import net.suby.designpattern.behavioral.strategy.example2.robot.Robot;
import net.suby.designpattern.behavioral.strategy.example2.robot.TaekwonV;
import net.suby.designpattern.behavioral.strategy.example2.strategies.FlyingStrategy;
import net.suby.designpattern.behavioral.strategy.example2.strategies.MissileStrategy;
import net.suby.designpattern.behavioral.strategy.example2.strategies.PunchStrategy;
import net.suby.designpattern.behavioral.strategy.example2.strategies.WalkingStrategy;

public class Execute {
    public static void main(String[] args) {
        Robot taekwonV = new TaekwonV("TaekwonV");
        Robot atom = new Atom("Atom");

        /* 수정된 부분: 전략 변경 방법 */
        taekwonV.setMovingStrategy(new WalkingStrategy());
        taekwonV.setAttackStrategy(new MissileStrategy());
        atom.setMovingStrategy(new FlyingStrategy());
        atom.setAttackStrategy(new PunchStrategy());

        /* 아래부터는 동일 */
        System.out.println("My name is " + taekwonV.getName());
        taekwonV.move();
        taekwonV.attack();

        System.out.println();
        System.out.println("My name is " + atom.getName());
        atom.move();
        atom.attack();

    }
}
