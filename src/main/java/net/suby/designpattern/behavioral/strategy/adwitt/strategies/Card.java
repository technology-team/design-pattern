package net.suby.designpattern.behavioral.strategy.adwitt.strategies;

public class Card implements PaymentMethod {
    @Override
    public void pay(int paymentAmount) {
        System.out.println("신용카드로 " + paymentAmount + "을 계산합니다.");
    }
}
