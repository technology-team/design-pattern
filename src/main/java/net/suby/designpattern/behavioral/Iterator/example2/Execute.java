package net.suby.designpattern.behavioral.Iterator.example2;

import java.util.Iterator;
import java.util.List;

import static java.util.Arrays.asList;

public class Execute {
    public static void main(String[] args) {
        List<String> list = asList("a", "b");

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
