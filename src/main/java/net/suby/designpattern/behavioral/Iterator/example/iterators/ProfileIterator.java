package net.suby.designpattern.behavioral.Iterator.example.iterators;

import net.suby.designpattern.behavioral.Iterator.example.social_networks.Profile;

public interface ProfileIterator {
    boolean hasNext();

    Profile getNext();

    void reset();
}