package net.suby.designpattern.behavioral.Iterator.example.social_networks;

import net.suby.designpattern.behavioral.Iterator.example.iterators.ProfileIterator;

public interface SocialNetwork {
    ProfileIterator createFriendsIterator(String profileEmail);

    ProfileIterator createCoworkersIterator(String profileEmail);
}