package net.suby.designpattern.behavioral.chainOfResponsibility.adwitt.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountProcessStepDto {
    private String userId;
    private int processCode;
}
