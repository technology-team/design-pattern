package net.suby.designpattern.behavioral.chainOfResponsibility.adwitt;

import net.suby.designpattern.behavioral.chainOfResponsibility.adwitt.dto.AccountProcessStepDto;

public abstract class AccountLinked {
    private AccountLinked accountLinked;

    public AccountLinked nextStep(AccountLinked accountLinked) {
        this.accountLinked = accountLinked;
        return accountLinked;
    }

    protected void nextStep(AccountProcessStepDto accountProcessStepDto) {
        if (accountLinked != null) {
            this.accountLinked.process(accountProcessStepDto);
        }
    }

    public abstract void process(AccountProcessStepDto accountProcessStepDto);
}
