package net.suby.designpattern.behavioral.chainOfResponsibility.adwitt;

import net.suby.designpattern.behavioral.chainOfResponsibility.adwitt.dto.AccountProcessStepDto;

public class CreateChildBusinessStep extends AccountLinked {
    private AccountProcessStepDto accountProcessStepDto;

    public CreateChildBusinessStep() {
    }

    @Override
    public void process(AccountProcessStepDto accountProcessStepDto) {
        // processCode  == 1
        int stepCode = accountProcessStepDto.getProcessCode();
        if (stepCode == 1) {
            System.out.println("create Business Manager");
            accountProcessStepDto.setProcessCode(++stepCode);
        }
        nextStep(accountProcessStepDto);
    }
}
