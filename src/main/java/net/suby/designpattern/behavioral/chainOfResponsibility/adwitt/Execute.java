package net.suby.designpattern.behavioral.chainOfResponsibility.adwitt;

import net.suby.designpattern.behavioral.chainOfResponsibility.adwitt.dto.AccountProcessStepDto;

public class Execute {
    private static AccountLinked accountLinked;

    public static void init() {


        AccountLinked createChildBusinessStep = new CreateChildBusinessStep();
        createChildBusinessStep.nextStep(new CreateAdAccountStep())
                .nextStep(new CreatePageStep());

        accountLinked = createChildBusinessStep;

    }

    public static void main(String[] args) {
        /*
        계정 연동 상태를 조회한다
        bm을 만든다.
        adAccount를 생성한다
        page를 연동한다.
         */
        init();
        AccountProcessStepDto accountProcessStepDto = new AccountProcessStepDto();
        accountProcessStepDto.setUserId("admitt");
        accountProcessStepDto.setProcessCode(1);
        accountLinked.process(accountProcessStepDto);
    }
}
