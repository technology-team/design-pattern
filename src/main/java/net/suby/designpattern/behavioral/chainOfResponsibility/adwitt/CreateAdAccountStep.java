package net.suby.designpattern.behavioral.chainOfResponsibility.adwitt;

import net.suby.designpattern.behavioral.chainOfResponsibility.adwitt.dto.AccountProcessStepDto;

public class CreateAdAccountStep extends AccountLinked {

    @Override
    public void process(AccountProcessStepDto accountProcessStepDto) {
        // processCode  == 2
        int stepCode = accountProcessStepDto.getProcessCode();
        if (stepCode == 2) {
            System.out.println("create AdAccount");
            accountProcessStepDto.setProcessCode(++stepCode);
        }
        nextStep(accountProcessStepDto);
    }
}
