package net.suby.designpattern.behavioral.chainOfResponsibility.adwitt;

import net.suby.designpattern.behavioral.chainOfResponsibility.adwitt.dto.AccountProcessStepDto;

public class CreatePageStep extends AccountLinked {

    @Override
    public void process(AccountProcessStepDto accountProcessStepDto) {
        // processCode  == 3
        int stepCode = accountProcessStepDto.getProcessCode();
        if (stepCode == 3) {
            System.out.println("create Page");
        }
        nextStep(accountProcessStepDto);
    }
}
