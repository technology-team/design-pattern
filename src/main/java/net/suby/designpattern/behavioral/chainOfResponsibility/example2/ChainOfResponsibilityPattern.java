package net.suby.designpattern.behavioral.chainOfResponsibility.example2;

public class ChainOfResponsibilityPattern {
    private DispenseChain c1;

    public ChainOfResponsibilityPattern() {
        this.c1 = new Dollar50Dispenser();
        DispenseChain c2 = new Dollar20Dispenser();
        DispenseChain c3 = new Dollar10Dispenser();

        c1.setNextChain(c2);
        c2.setNextChain(c3);
    }

    public DispenseChain getDispenseChain() {
        return this.c1;
    }
}
