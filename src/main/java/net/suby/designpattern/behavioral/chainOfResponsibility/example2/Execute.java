package net.suby.designpattern.behavioral.chainOfResponsibility.example2;

import java.util.Scanner;

public class Execute {
    public static void main(String[] args) {
        ChainOfResponsibilityPattern atmDispenser = new ChainOfResponsibilityPattern();
        Scanner input = null;
        while (true) {
            int amount = 0;
            System.out.println("Enter amount to dispense");
            input = new Scanner(System.in);
            amount = input.nextInt();
            if (amount % 10 != 0) {
                System.out.println("Amount should be multiple of ten! Input again!");
                continue;
            }
            atmDispenser.getDispenseChain().dispense(new Currency(amount));
        }
    }

}
