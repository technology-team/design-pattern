package net.suby.designpattern.behavioral.chainOfResponsibility.example2;

public interface DispenseChain {
    void setNextChain(DispenseChain nextChain);

    void dispense(Currency cur);
}
