package net.suby.designpattern.behavioral.mediator.example;

public interface IDestination {
    public void receiveEvent(String from, String event);
}
