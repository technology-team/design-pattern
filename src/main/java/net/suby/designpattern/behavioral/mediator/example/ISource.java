package net.suby.designpattern.behavioral.mediator.example;

public interface ISource {
    public void setMediator(Mediator mediator);

    public void eventOccured(String event);

}
