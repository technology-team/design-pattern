package net.suby.designpattern.behavioral.state.example2;

public class Light {
    private State state;

    public Light() {
        state = OffState.getInstance();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void onButton() {
        this.state.onButton(this);
    }

    public void offButton() {
        this.state.offButton(this);
    }
}
