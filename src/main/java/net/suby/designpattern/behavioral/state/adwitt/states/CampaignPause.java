package net.suby.designpattern.behavioral.state.adwitt.states;

public class CampaignPause implements CampaignState {
    @Override
    public void onActive(Campaign campaign) {
        System.out.println("캠페인이 활성상태로 변경되었습니다.");
        campaign.changeCampaignState(new CampaignActive());
    }

    @Override
    public void onPause(Campaign campaign) {
        System.out.println("이미 대기상태입니다.");
    }

    @Override
    public void onDelete(Campaign campaign) {
        System.out.println("캠페인이 삭제되었습니다.");
        campaign.changeCampaignState(new CampaignDelete());
    }
}
