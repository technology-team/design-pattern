package net.suby.designpattern.behavioral.state.adwitt.states;

public class Campaign {
    private CampaignState campaignState;

    public Campaign() {
        this.campaignState = new CampaignActive();
    }

    public void changeCampaignState(CampaignState campaignState) {
        this.campaignState = campaignState;
    }

    public void onActive() {
        this.campaignState.onActive(this);
    }

    public void onPause() {
        this.campaignState.onPause(this);
    }

    public void onDelete() {
        this.campaignState.onDelete(this);
    }
}
