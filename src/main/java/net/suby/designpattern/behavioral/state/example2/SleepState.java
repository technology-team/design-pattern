package net.suby.designpattern.behavioral.state.example2;

public class SleepState implements State {

    private SleepState() {}

    public static SleepState getInstance() {
        return LazyHolder.SLEEP;
    }

    @Override
    public void onButton(Light light) {
        light.setState(OnState.getInstance());
        System.out.println("잠자기 모드 해제!(ON 상태)");
    }

    @Override
    public void offButton(Light light) {
        light.setState(OffState.getInstance());
        System.out.println("불 꺼짐!");
    }


    private static class LazyHolder {
        public static final SleepState SLEEP = new SleepState();
    }

}
