package net.suby.designpattern.behavioral.state.example2;

public class OffState implements State {

    private OffState() {}

    public static OffState getInstance() {
        return LazyHolder.OFF;
    }

    @Override
    public void onButton(Light light) {
        light.setState(OnState.getInstance());
        System.out.println("불 켜짐!");
    }

    @Override
    public void offButton(Light light) {
        System.out.println("동작 없음!");
    }


    private static class LazyHolder {
        public static final OffState OFF = new OffState();
    }
}
