package net.suby.designpattern.behavioral.state.example2;

public class Execute {
    public static void main(String[] args) {
        Light l = new Light();

        l.offButton();
        l.onButton();
        l.onButton();
        l.onButton();
        l.offButton();
        l.onButton();
        l.offButton();
    }
}
