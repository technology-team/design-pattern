package net.suby.designpattern.behavioral.state.adwitt.states;

public class CampaignActive implements CampaignState {

    @Override
    public void onActive(Campaign campaign) {
        System.out.println("이미 활성화중인 캠페인입니다.");
    }

    @Override
    public void onPause(Campaign campaign) {
        System.out.println("캠페인이 대기상태로 변경되었습니다.");
        campaign.changeCampaignState(new CampaignPause());
    }

    @Override
    public void onDelete(Campaign campaign) {
        System.out.println("캠페인이 삭제되었습니다.");
        campaign.changeCampaignState(new CampaignDelete());
    }
}
