package net.suby.designpattern.behavioral.state.adwitt.states;

public class CampaignDelete implements CampaignState {
    @Override
    public void onActive(Campaign campaign) {
        System.out.println("삭제된 캠페인입니다.");
    }

    @Override
    public void onPause(Campaign campaign) {
        System.out.println("삭제된 캠페인입니다.");
    }

    @Override
    public void onDelete(Campaign campaign) {
        System.out.println("이미 캠페인이 삭제되었습니다.");
    }
}
