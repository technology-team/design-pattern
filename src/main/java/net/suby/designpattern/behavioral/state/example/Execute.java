package net.suby.designpattern.behavioral.state.example;

import net.suby.designpattern.behavioral.state.example.ui.Player;
import net.suby.designpattern.behavioral.state.example.ui.UI;

public class Execute {
    public static void main(String[] args) {
        Player player = new Player();
        UI ui = new UI(player);
        ui.init();
    }
}
