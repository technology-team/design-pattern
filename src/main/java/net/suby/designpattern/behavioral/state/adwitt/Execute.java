package net.suby.designpattern.behavioral.state.adwitt;

import net.suby.designpattern.behavioral.state.adwitt.states.Campaign;

public class Execute {
    /*
    상태패턴은 backend보다 front에서 많이 사용될것으로 보인다.
     */
    public static void main(String[] args) {
        Campaign campaignState = new Campaign();
        campaignState.onActive();
        campaignState.onPause();
        //campaignState.changeCampaignState(new CampaignPause());
        campaignState.onPause();
        campaignState.onDelete();
        //campaignState.changeCampaignState(new CampaignDelete());
        campaignState.onActive();
    }
}
