package net.suby.designpattern.behavioral.state.example2;

public interface State {
    public void onButton(Light light);

    public void offButton(Light light);
}
