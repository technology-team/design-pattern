package net.suby.designpattern.behavioral.state.adwitt.states;

public interface CampaignState {
    public void onActive(Campaign campaign);

    public void onPause(Campaign campaign);

    public void onDelete(Campaign campaign);
}
