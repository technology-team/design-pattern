package net.suby.designpattern.behavioral.memento.example;

public class Execute {

    /*
    메멘토 패턴은 객체의 상태 정보를 저장하고 사용자의 필요에 의하여 원하는 시점의 데이터를 복원 할 수 있는 패턴을 의미합니다.

    텍스터나 ide 등에서 ctrl + z 나 command + z를 통해 과거의 상태로 돌아갈 수 있게 해줍니다.
    이러한 상황에서도 보통 Memento Pattern을 사용하게 됩니다.
     */

    public static void main(String[] args) {
        Gamer gamer = new Gamer(100);
        Memento memento = gamer.createMemento();

        for (int i = 0; i < 100; i++) {
            System.out.println("==== " + i);
            System.out.println("현상:" + gamer);
            gamer.bet();

            System.out.println("소지금은 " + gamer.getMoney() + "원이 입력되었습니다.");


            if (gamer.getMoney() > memento.getMoney()) {
                System.out.println("많이 증가했으므로 현재의 상태를 저장하자!");
                memento = gamer.createMemento();
            } else if (gamer.getMoney() < memento.getMoney() / 2) {
                System.out.println("많이 감소했으므로 이전의 상태로 복원하자");
                gamer.restoreMemento(memento);
            }
        }
    }
}
