package net.suby.designpattern.behavioral.observer.adwitt.listeners;

public interface NotificationListener {
    public void update(String eventType);
}
