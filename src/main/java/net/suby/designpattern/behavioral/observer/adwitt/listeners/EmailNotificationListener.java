package net.suby.designpattern.behavioral.observer.adwitt.listeners;

public class EmailNotificationListener implements NotificationListener {
    private String email;

    public EmailNotificationListener(String email) {
        this.email = email;
    }

    @Override
    public void update(String eventType) {
        System.out.println(this.email + "로 이메일을 보냅니다.(" + eventType + ")");
    }
}
