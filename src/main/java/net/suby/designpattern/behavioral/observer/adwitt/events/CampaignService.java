package net.suby.designpattern.behavioral.observer.adwitt.events;

import net.suby.designpattern.behavioral.observer.adwitt.listeners.EmailNotificationListener;
import net.suby.designpattern.behavioral.observer.adwitt.listeners.SlackNotificationListener;
import net.suby.designpattern.behavioral.observer.adwitt.publisher.EventManager;

public class CampaignService {
    private EventManager eventManager;

    public CampaignService() {
        this.eventManager = new EventManager("update", "create");
        this.eventManager.subscribe("update", new EmailNotificationListener("myborn@adwitt.com"));
        this.eventManager.subscribe("update", new SlackNotificationListener("updateChannel"));
        this.eventManager.subscribe("create", new SlackNotificationListener("createChannel"));
    }

    public void updateCampaign() {
        System.out.println("캠페인 정보가 update 되었습니다.");
        eventManager.notify("update");
    }

    public void createCampaign() {
        System.out.println("캠페인 정보가 create 되었습니다.");
        eventManager.notify("create");
    }
}
