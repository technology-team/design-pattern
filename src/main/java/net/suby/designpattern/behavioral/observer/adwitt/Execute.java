package net.suby.designpattern.behavioral.observer.adwitt;

import net.suby.designpattern.behavioral.observer.adwitt.events.CampaignService;

public class Execute {
    public static void main(String[] args) {
        CampaignService campaignService = new CampaignService();

        campaignService.createCampaign();
        campaignService.updateCampaign();
    }
}
