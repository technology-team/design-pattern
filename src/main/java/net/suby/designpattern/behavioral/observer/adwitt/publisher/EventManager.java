package net.suby.designpattern.behavioral.observer.adwitt.publisher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.suby.designpattern.behavioral.observer.adwitt.listeners.NotificationListener;

public class EventManager {
    Map<String, List<NotificationListener>> listeners = new HashMap<>();

    public EventManager(String... operations) {
        for (String operation : operations) {
            this.listeners.put(operation, new ArrayList<>());
        }
    }

    public void subscribe(String eventType, NotificationListener listener) {
        List<NotificationListener> _listeners = this.listeners.get(eventType);
        _listeners.add(listener);
    }

    public void unsubscribe(String eventType, NotificationListener listener) {
        List<NotificationListener> _listeners = this.listeners.get(eventType);
        _listeners.remove(listener);
    }

    public void notify(String eventType) {
        List<NotificationListener> _listeners = listeners.get(eventType);
        for (NotificationListener listener : _listeners) {
            listener.update(eventType);
        }
    }
}
