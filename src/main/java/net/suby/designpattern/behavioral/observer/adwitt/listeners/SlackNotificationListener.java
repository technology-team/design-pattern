package net.suby.designpattern.behavioral.observer.adwitt.listeners;

public class SlackNotificationListener implements NotificationListener {
    private String channel;

    public SlackNotificationListener(String channel) {
        this.channel = channel;
    }

    @Override
    public void update(String eventType) {
        System.out.println("slack " + this.channel + " 로 알람을 보냅니다.(" + eventType + ")");
    }
}
