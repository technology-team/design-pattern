package net.suby.designpattern.behavioral.command.example2;

public interface Command {
    public abstract void execute();
}
