package net.suby.designpattern.behavioral.command.example2;

public class Lamp {
    public void turnOn() {
        System.out.println("Lamp On");
    }

}
