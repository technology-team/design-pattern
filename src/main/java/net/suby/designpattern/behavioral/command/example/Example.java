package net.suby.designpattern.behavioral.command.example;

import net.suby.designpattern.behavioral.command.example.editor.Editor;

public class Example {
    public static void main(String[] args) {
        Editor editor = new Editor();
        editor.init();
    }
}
