package net.suby.designpattern.behavioral.command.example2;

public class Alarm {
    public void start() {
        System.out.println("Alarming");
    }
}
