package net.suby.designpattern.structural.facade.example;

public class Execute {
    // 음료를 준비한다 -> TV를 켠다 -> 영화를 검색한다 -> 영화를 결제한다 -> 영화를 재생한다.
    //    public void view()
    //    {
    //        Beverage beverage = new Beverage("콜라");
    //        Remote_Control remote= new Remote_Control();
    //        Movie movie = new Movie("어벤져스");
    //
    //        beverage.Prepare();  //음료 준비
    //        remote.Turn_On();   //tv를 켜다
    //        movie.Search_Movie();  //영화를 찾다
    //        movie.Charge_Movie();  // 영화를 결제하다
    //        movie.play_Movie();   //영화를 재생하다

    public static void main(String[] args) {
        Facade facade = new Facade("popcon", "joker");
        facade.viewMovie();
    }


    //    }


    // ref: Wikipedia
    //    class CPU {
    //        public void freeze() { ... }
    //        public void jump(long position) { ... }
    //        public void execute() { ... }
    //    }
    //
    //    class Memory {
    //        public void load(long position, byte[] data) {
    //        ...
    //        }
    //    }
    //
    //    class HardDrive {
    //        public byte[] read(long lba, int size) {
    //        ...
    //        }
    //    }
    //
    //    class Client {
    //        public static void main(String[] args) throws ParseException {
    //            // Start Computer
    //            CPU cpu = new CPU();
    //            Memory memory = new Memory();
    //            HardDrive hardDrive = new HardDrive();
    //            cpu.freeze();
    //            memory.load(BOOT_ADDRESS, hardDrive.read(BOOT_SECTOR, SECTOR_SIZE));
    //            cpu.jump(BOOT_ADDRESS);
    //            cpu.execute();
    //        }
    //    }

    //    class CPU {
    //        public void freeze() { ... }
    //        public void jump(long position) { ... }
    //        public void execute() { ... }
    //    }
    //
    //    class Memory {
    //        public void load(long position, byte[] data) {
    //        ...
    //        }
    //    }
    //
    //    class HardDrive {
    //        public byte[] read(long lba, int size) {
    //        ...
    //        }
    //    }
    //
    //    class Computer {
    //        public void startComputer() {
    //            CPU cpu = new CPU();
    //            Memory memory = new Memory();
    //            HardDrive hardDrive = new HardDrive();
    //            cpu.freeze();
    //            memory.load(BOOT_ADDRESS, hardDrive.read(BOOT_SECTOR, SECTOR_SIZE));
    //            cpu.jump(BOOT_ADDRESS);
    //            cpu.execute();
    //        }
    //    }
    //
    //    class Client {
    //        public static void main(String[] args) throws ParseException {
    //            Computer facade = /* grab a facade instance */;
    //            facade.startComputer();
    //        }
    //    }

}
