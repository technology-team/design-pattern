package net.suby.designpattern.structural.facade.adwitt;

public class FacebookCall {

    private int spend;

    public FacebookCall(int spend) {
        this.spend = spend;
    }

    public int getSpend() {
        System.out.println(String.format("facebook에서 총소진액 %d을 조회하였습니다.", this.spend));
        return this.spend;
    }

    public void setSpendCap(int spendCap) {
        System.out.println(String.format("facebook에 spendCap을 %d으로 설정하였습니다.", spendCap));
    }
}
