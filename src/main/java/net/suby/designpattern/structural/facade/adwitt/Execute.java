package net.suby.designpattern.structural.facade.adwitt;

public class Execute {
    public static void main(String[] args) {
        /*
        1. 토탈 충전 금액을 조회한다.(DB)
        2. 현재까지의 모든 소진금액을 조회한다.
        3. 충전금액을 계산한다. 토탈충전금액 - 소진금액
        4. spendCap을 수정한다.
        5. db에 해당 정보를 저장한다.
         */
        ChargeFacade chargeFacade = new ChargeFacade();
        chargeFacade.chargeProcess();

    }
}
