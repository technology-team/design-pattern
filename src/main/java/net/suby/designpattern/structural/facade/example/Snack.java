package net.suby.designpattern.structural.facade.example;

public class Snack {
    private String name = "";

    public Snack(String name) {
        this.name = name;
    }

    public void prepare() {
        System.out.println(name + " 간식 준비 완료 ");
    }
}
