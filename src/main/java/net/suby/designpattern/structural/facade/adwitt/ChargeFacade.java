package net.suby.designpattern.structural.facade.adwitt;

public class ChargeFacade {

    public void chargeProcess() {
        ChargeService chargeService = new ChargeService(50000);
        FacebookCall facebookCall = new FacebookCall(25400);
        int spendCap = Calculate.minus(chargeService.getTotalCharge(), facebookCall.getSpend());
        facebookCall.setSpendCap(spendCap);
        chargeService.saveCharge(spendCap);
    }
}
