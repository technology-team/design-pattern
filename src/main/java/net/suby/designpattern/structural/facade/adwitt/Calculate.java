package net.suby.designpattern.structural.facade.adwitt;

public class Calculate {

    public static int minus(int a, int b) {
        System.out.println(String.format("총 충전금액 %d - 총소진액 %d를 계산합니다.", a, b));
        return a - b;
    }
}
