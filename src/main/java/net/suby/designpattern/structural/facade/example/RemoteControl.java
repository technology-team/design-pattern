package net.suby.designpattern.structural.facade.example;

public class RemoteControl {
    public void turnOn() {
        System.out.println("TV를 켜다");
    }

    public void turnOff() {
        System.out.println("TV를 끄다");
    }
}
