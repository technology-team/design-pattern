package net.suby.designpattern.structural.facade.adwitt;

public class ChargeService {
    private int totalCharge;

    public ChargeService(int totalCharge) {
        this.totalCharge = totalCharge;
    }

    public int getTotalCharge() {
        System.out.println(String.format("db에서 총 충전금액 %d를 조회했습니다.", this.totalCharge));
        return this.totalCharge;
    }

    public void saveCharge(int spendCap) {
        System.out.println(String.format("db에 spendCap %d변경을 저장", spendCap));
    }
}
