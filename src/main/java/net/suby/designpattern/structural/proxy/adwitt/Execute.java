package net.suby.designpattern.structural.proxy.adwitt;

public class Execute {
    public static void main(String[] args) {
        AdwittUserProxy adwittUserProxy = new AdwittUserProxy();
        AdwittUser adwittUser = adwittUserProxy.getAdwittUser("token");
        System.out.println(adwittUser);

        adwittUser = adwittUserProxy.getAdwittUser(null);
        System.out.println(adwittUser);
    }
}
