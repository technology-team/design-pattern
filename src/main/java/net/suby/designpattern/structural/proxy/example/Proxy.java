package net.suby.designpattern.structural.proxy.example;

public class Proxy implements Something {
    Something something;

    @Override
    public String runSomething() {
        System.out.println("호출에 대한 흐름 제어가 주목적, 반환 결과를 그대로 전달");

        something = new SomethingRun();
        return this.something.runSomething();
    }
}
