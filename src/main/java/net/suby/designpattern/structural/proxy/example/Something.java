package net.suby.designpattern.structural.proxy.example;

public interface Something {
    String runSomething();
}
