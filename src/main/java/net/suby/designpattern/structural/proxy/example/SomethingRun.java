package net.suby.designpattern.structural.proxy.example;

public class SomethingRun implements Something {
    @Override
    public String runSomething() {
        return "무엇을 실행합니다.";
    }
}
