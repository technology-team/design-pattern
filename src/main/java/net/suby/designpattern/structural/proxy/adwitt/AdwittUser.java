package net.suby.designpattern.structural.proxy.adwitt;

import lombok.Data;

@Data
public class AdwittUser {
    private String id;
    private String name;
}
