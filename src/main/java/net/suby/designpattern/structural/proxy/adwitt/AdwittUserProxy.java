package net.suby.designpattern.structural.proxy.adwitt;

public class AdwittUserProxy implements IAdwittUserInfo {

    @Override
    public AdwittUser getAdwittUser(String token) {
        if (isValidToken(token)) {
            IAdwittUserInfo adwittUserInfo = new AdwittUserInfo();
            return adwittUserInfo.getAdwittUser(token);
        }
        return new AdwittUser();
    }

    // token에 대한 유효성 체크
    private boolean isValidToken(String token) {
        if (token == null) {
            return false;
        }

        return true;
    }
}
