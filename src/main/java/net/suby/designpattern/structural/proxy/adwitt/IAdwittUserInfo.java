package net.suby.designpattern.structural.proxy.adwitt;

public interface IAdwittUserInfo {
    public AdwittUser getAdwittUser(String token);
}
