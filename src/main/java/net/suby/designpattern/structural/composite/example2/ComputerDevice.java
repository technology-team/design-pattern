package net.suby.designpattern.structural.composite.example2;

// Component
public interface ComputerDevice {
    public int getPrice();

    public int getPower();
}
