package net.suby.designpattern.structural.composite.adwitt;

public class AdsetInsight implements InsightLeaf {
    private String adsetId;
    private String accountId;

    public AdsetInsight(String adsetId, String accountId) {
        this.adsetId = adsetId;
        this.accountId = accountId;
    }

    @Override
    public void getInsight() {
        System.out.println("adsetId의 campaign insight");
    }

    @Override
    public void getInsightByAccountId() {
        System.out.println("adAccount의 모든 adset insight");
    }
}
