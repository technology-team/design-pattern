package net.suby.designpattern.structural.composite.example;

public class Line implements Shape {

    @Override
    public void draw(String color) {
        System.out.println("line color: " + color);
    }
}
