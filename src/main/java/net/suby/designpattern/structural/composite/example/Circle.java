package net.suby.designpattern.structural.composite.example;

public class Circle implements Shape {
    @Override
    public void draw(String color) {
        System.out.println("circle color: " + color);
    }
}
