package net.suby.designpattern.structural.composite.adwitt;

// leaf
public class CampaignInsight implements InsightLeaf {
    private String campaignId;
    private String accountId;

    public CampaignInsight(String campaignId, String accountId) {
        this.campaignId = campaignId;
        this.accountId = accountId;
    }

    @Override
    public void getInsight() {
        System.out.println("campaignId의 campaign insight");
    }

    @Override
    public void getInsightByAccountId() {
        System.out.println("adAccount의 모든 campaign insight");
    }
}
