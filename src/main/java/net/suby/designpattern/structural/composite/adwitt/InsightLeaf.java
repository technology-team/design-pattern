package net.suby.designpattern.structural.composite.adwitt;

// Component
public interface InsightLeaf {
    public void getInsight();

    public void getInsightByAccountId();
}
