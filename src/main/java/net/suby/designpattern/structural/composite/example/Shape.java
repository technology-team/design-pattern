package net.suby.designpattern.structural.composite.example;

// Component
public interface Shape {
    public void draw(String color);
}
