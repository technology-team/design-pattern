package net.suby.designpattern.structural.composite.adwitt;

import static java.util.Objects.nonNull;

// composite
public class CompositeInsight implements InsightLeaf {
    private CampaignInsight campaignInsight;
    private AdsetInsight adsetInsight;
    private AdInsight adInsight;

    public void addCampaignInsight(CampaignInsight campaignInsight) {
        this.campaignInsight = campaignInsight;
    }

    public void addAdsetInsight(AdsetInsight adsetInsight) {
        this.adsetInsight = adsetInsight;
    }

    public void addAdInsight(AdInsight adInsight) {
        this.adInsight = adInsight;
    }

    @Override
    public void getInsight() {
        if (nonNull(this.campaignInsight)) {
            this.campaignInsight.getInsight();
        }
        if (nonNull(this.adsetInsight)) {
            this.adsetInsight.getInsight();
        }
        if (nonNull(this.adInsight)) {
            this.adInsight.getInsight();
        }
    }

    @Override
    public void getInsightByAccountId() {
        if (nonNull(this.campaignInsight)) {
            this.campaignInsight.getInsightByAccountId();
        }
        if (nonNull(this.adsetInsight)) {
            this.adsetInsight.getInsightByAccountId();
        }
        if (nonNull(this.adInsight)) {
            this.adInsight.getInsightByAccountId();
        }
    }
}
