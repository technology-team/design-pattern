package net.suby.designpattern.structural.composite.example2;

// Leaf
public class Keyboard implements ComputerDevice {
    private int price;
    private int power;

    public Keyboard(int power, int price) {
        this.power = power;
        this.price = price;
    }

    public int getPrice() { return price; }

    public int getPower() { return power; }
}
