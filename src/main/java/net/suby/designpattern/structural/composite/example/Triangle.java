package net.suby.designpattern.structural.composite.example;

public class Triangle implements Shape {
    @Override
    public void draw(String color) {
        System.out.println("triangle color: " + color);
    }
}
