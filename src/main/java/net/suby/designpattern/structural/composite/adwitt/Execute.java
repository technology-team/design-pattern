package net.suby.designpattern.structural.composite.adwitt;

public class Execute {
    public static void main(String[] args) {
        CompositeInsight dashboard = new CompositeInsight();
        dashboard.addCampaignInsight(new CampaignInsight(null, "456789"));
        dashboard.getInsightByAccountId();

        System.out.println();
        CompositeInsight manage = new CompositeInsight();
        manage.addCampaignInsight(new CampaignInsight("123456", null));
        manage.addAdsetInsight(new AdsetInsight("123456", null));
        manage.addAdInsight(new AdInsight("123456", null));
        manage.getInsight();
    }
}
