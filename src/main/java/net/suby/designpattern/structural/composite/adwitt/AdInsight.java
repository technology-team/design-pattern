package net.suby.designpattern.structural.composite.adwitt;

public class AdInsight implements InsightLeaf {
    private String adId;
    private String accountId;

    public AdInsight(String adId, String accountId) {
        this.adId = adId;
        this.accountId = accountId;
    }

    @Override
    public void getInsight() {
        System.out.println("adId의 Ad insight");
    }

    @Override
    public void getInsightByAccountId() {
        System.out.println("adAccount의 모든 ad insight");
    }
}
