package net.suby.designpattern.structural.bridge.adwitt;

public class Manage implements InsightHandler {
    private Insight insight;

    public Manage(Insight insight) {
        this.insight = insight;
    }

    @Override
    public void handle() {
        insight.getInsightById();
    }
}
