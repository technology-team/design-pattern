package net.suby.designpattern.structural.bridge.adwitt;

public interface InsightHandler {
    public void handle();
}
