package net.suby.designpattern.structural.bridge.adwitt;

public class CampaignInsight implements Insight {
    @Override
    public void getInsightByAccountId() {
        System.out.println("모든 campaign insight를 조회합니다.");
    }

    @Override
    public void getInsightById() {
        System.out.println("campaign Id insight를 조회합니다.");
    }
}
