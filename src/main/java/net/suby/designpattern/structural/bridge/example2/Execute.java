package net.suby.designpattern.structural.bridge.example2;

import net.suby.designpattern.structural.bridge.example2.devices.Device;
import net.suby.designpattern.structural.bridge.example2.devices.Radio;
import net.suby.designpattern.structural.bridge.example2.devices.Tv;
import net.suby.designpattern.structural.bridge.example2.remotes.AdvancedRemote;
import net.suby.designpattern.structural.bridge.example2.remotes.BasicRemote;

public class Execute {
    public static void main(String[] args) {
        testDevice(new Tv());
        testDevice(new Radio());
    }

    public static void testDevice(Device device) {
        System.out.println("Tests with basic remote.");
        BasicRemote basicRemote = new BasicRemote(device);
        basicRemote.power();
        device.printStatus();

        System.out.println("Tests with advanced remote.");
        AdvancedRemote advancedRemote = new AdvancedRemote(device);
        advancedRemote.power();
        advancedRemote.mute();
        device.printStatus();
    }
}
