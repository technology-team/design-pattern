package net.suby.designpattern.structural.bridge.example;

// 기능 클래스 계층
public class Smith implements WeaponHandler {
    private Weapon weapon;

    public Smith(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public void handle() {
        System.out.println("I'm smith");
        weapon.repair();
    }
}
