package net.suby.designpattern.structural.bridge.example;

// 구현 클래스 계층
public interface Weapon {

    public void attack();

    public void repair();
}
