package net.suby.designpattern.structural.bridge.adwitt;

public class AdInsight implements Insight {
    @Override
    public void getInsightByAccountId() {
        System.out.println("모든 ad insight를 조회합니다.");
    }

    @Override
    public void getInsightById() {
        System.out.println("ad Id로 insight를 조회합니다.");
    }
}
