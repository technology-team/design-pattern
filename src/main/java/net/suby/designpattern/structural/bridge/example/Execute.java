package net.suby.designpattern.structural.bridge.example;

public class Execute {

    public static void main(String[] args) {
        WeaponHandler weaponHandler = new Warrior(new Sword());
        weaponHandler.handle();


        weaponHandler = new Smith(new Bow());
        weaponHandler.handle();

        /*
        이러한 방식으로 무기의 종류를 늘리거나 무기를 다루는 사람을 늘리더라도 다른 한 쪽에는 변경에 영향을 미치지 않도록 만들어 준다.
        */
    }
}
