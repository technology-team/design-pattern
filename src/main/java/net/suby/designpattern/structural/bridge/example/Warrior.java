package net.suby.designpattern.structural.bridge.example;

// 기능 클래스 계층
public class Warrior implements WeaponHandler {
    private Weapon weapon;

    public Warrior(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public void handle() {
        System.out.println("I'm warrior~~");
        weapon.attack();
    }
}
