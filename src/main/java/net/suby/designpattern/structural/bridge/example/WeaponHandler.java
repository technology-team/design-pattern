package net.suby.designpattern.structural.bridge.example;

// 기능 클래스 계층
public interface WeaponHandler {
    public void handle();
}
