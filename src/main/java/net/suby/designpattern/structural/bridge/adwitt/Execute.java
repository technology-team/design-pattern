package net.suby.designpattern.structural.bridge.adwitt;

public class Execute {
    public static void main(String[] args) {
        InsightHandler insightHandler = new Dashboard(new CampaignInsight());
        insightHandler.handle();

        insightHandler = new Manage(new CampaignInsight());
        insightHandler.handle();
    }
}
