package net.suby.designpattern.structural.bridge.example;

// 구현 클래스 계층
public class Bow implements Weapon {
    @Override
    public void attack() {
        System.out.println("활로 공격 슝~");
    }

    @Override
    public void repair() {
        System.out.println("활수리 콩콩콩~");
    }
}
