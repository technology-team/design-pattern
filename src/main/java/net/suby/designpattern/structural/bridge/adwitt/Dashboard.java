package net.suby.designpattern.structural.bridge.adwitt;

public class Dashboard implements InsightHandler {
    private Insight insight;

    public Dashboard(Insight insight) {
        this.insight = insight;
    }

    @Override
    public void handle() {
        insight.getInsightByAccountId();
    }
}
