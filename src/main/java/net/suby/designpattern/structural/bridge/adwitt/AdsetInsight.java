package net.suby.designpattern.structural.bridge.adwitt;

public class AdsetInsight implements Insight {
    @Override
    public void getInsightByAccountId() {
        System.out.println("모든 adset insight를 조회합니다.");
    }

    @Override
    public void getInsightById() {
        System.out.println("adset Id로 insight를 조회합니다.");
    }
}
