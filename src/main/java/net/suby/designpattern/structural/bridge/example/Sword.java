package net.suby.designpattern.structural.bridge.example;

// 구현 클래스 계층
public class Sword implements Weapon {
    @Override
    public void attack() {
        System.out.println("검을 휘두른다 슉~");
    }

    @Override
    public void repair() {
        System.out.println("검을 수리한다 땅땅땅~");
    }
}
