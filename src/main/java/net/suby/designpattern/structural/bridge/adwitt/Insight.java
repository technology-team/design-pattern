package net.suby.designpattern.structural.bridge.adwitt;

public interface Insight {
    public void getInsightByAccountId();

    public void getInsightById();
}
