package net.suby.designpattern.structural.flyweight.example;

public class Subject {
    private String field;

    public Subject(String field) {
        this.field = field;
    }

    public String getField() {
        return this.field;
    }
}
