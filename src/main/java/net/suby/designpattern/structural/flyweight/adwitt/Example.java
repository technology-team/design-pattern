package net.suby.designpattern.structural.flyweight.adwitt;

public class Example {
    public static void main(String[] args) {
        AdwittUser adwittUser = AdwittFlyweightFactory.getAdwittUser("1");
        System.out.println(adwittUser.toString());
        adwittUser = AdwittFlyweightFactory.getAdwittUser("2");
        System.out.println(adwittUser.toString());
        adwittUser = AdwittFlyweightFactory.getAdwittUser("1");
        System.out.println(adwittUser.toString());
    }

}
