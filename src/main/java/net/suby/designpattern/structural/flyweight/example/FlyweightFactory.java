package net.suby.designpattern.structural.flyweight.example;

import java.util.HashMap;
import java.util.Map;

public class FlyweightFactory {
    private static Map<String, Subject> map = new HashMap<String, Subject>();

    public static Subject getSubject(String key) {
        Subject subject = map.get(key);
        if (subject == null) {
            subject = new Subject(key);
        }
        return subject;
    }
}
