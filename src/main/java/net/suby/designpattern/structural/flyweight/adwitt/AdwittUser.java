package net.suby.designpattern.structural.flyweight.adwitt;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class AdwittUser {
    private String id;
    private String name;
    private String role;

    public AdwittUser(String id, String name, String role) {
        this.id = id;
        this.name = name;
        this.role = role;
    }

}
