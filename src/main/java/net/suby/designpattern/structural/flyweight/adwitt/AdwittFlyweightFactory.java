package net.suby.designpattern.structural.flyweight.adwitt;

import java.util.HashMap;
import java.util.Map;

public class AdwittFlyweightFactory {
    private static Map<String, AdwittUser> map = new HashMap<String, AdwittUser>();

    public static AdwittUser getAdwittUser(String key) {
        AdwittUser adwittUser = map.get(key);
        if (adwittUser == null) {
            adwittUser = new AdwittUser(key, key + "name", "ADMIN");
        }
        return adwittUser;
    }

}
