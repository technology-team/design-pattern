package net.suby.designpattern.structural.flyweight.example;

public class Example {
    public static void main(String[] args) {
        Subject subject = FlyweightFactory.getSubject("5");
        System.out.println(subject.getField());
    }
}
