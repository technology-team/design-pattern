package net.suby.designpattern.structural.decorator.adwitt;

public class BudgetDecorator extends Budget {
    private Budget budgetDecorator;

    public BudgetDecorator(Budget budgetDecorator){
        this.budgetDecorator = budgetDecorator;
    }

    @Override
    public int getBudget() {
        return this.budgetDecorator.getBudget();
    }
}
