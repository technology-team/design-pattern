package net.suby.designpattern.structural.decorator.adwitt;

public abstract class Budget {
    private int budgetValue;

    public int getBudget() {
        return this.budgetValue;
    }
}
