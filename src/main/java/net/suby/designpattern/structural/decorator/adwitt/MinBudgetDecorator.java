package net.suby.designpattern.structural.decorator.adwitt;

public class MinBudgetDecorator extends Budget {
    private Budget wonBudgetDecorator;

    public MinBudgetDecorator(Budget wonBudgetDecorator) {
        this.wonBudgetDecorator = wonBudgetDecorator;
    }

    @Override
    public int getBudget() {
        int DEFAULT_BUDGET = 50000;
        if(this.wonBudgetDecorator.getBudget() < DEFAULT_BUDGET){
            return DEFAULT_BUDGET;
        }
        return this.wonBudgetDecorator.getBudget();
    }
}
