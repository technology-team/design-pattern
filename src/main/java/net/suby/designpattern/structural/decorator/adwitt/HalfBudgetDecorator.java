package net.suby.designpattern.structural.decorator.adwitt;

public class HalfBudgetDecorator extends Budget {
    private Budget halfBudgetDecorator;

    public HalfBudgetDecorator(Budget halfBudgetDecorator) {
        this.halfBudgetDecorator = halfBudgetDecorator;
    }

    @Override
    public int getBudget() {
        return this.halfBudgetDecorator.getBudget() / 2;
    }
}
