package net.suby.designpattern.structural.decorator.adwitt;

public class DefaultBudgetDecorator extends Budget {
    @Override
    public int getBudget() {
        return 10000;
    }
}
