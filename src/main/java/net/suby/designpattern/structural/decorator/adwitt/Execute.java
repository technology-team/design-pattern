package net.suby.designpattern.structural.decorator.adwitt;

public class Execute {
    public static void main(String[] args) {
        Budget budget = new DefaultBudgetDecorator();
        System.out.println(budget.getBudget());

        Budget halfBudget = new HalfBudgetDecorator(budget);
        System.out.println(halfBudget.getBudget());

        Budget minBudget = new MinBudgetDecorator(halfBudget);
        System.out.println(minBudget.getBudget());
    }
}
