package net.suby.designpattern.structural.decorator.example;

// 다양한 추가 기능에 대한 공통 클래스
public class DisplayDecorator implements Display {
    private Display decoratedDisplay;

    // 합성(composition)을 통해 RoadDiplay 객체에 대한 참조
    public DisplayDecorator(Display decoratedDisplay) {
        this.decoratedDisplay = decoratedDisplay;
    }

    @Override
    public void draw() {
        decoratedDisplay.draw();
    }
}
