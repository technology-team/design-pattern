package net.suby.designpattern.structural.decorator.example;

public interface Display {
    public void draw();
}
