package net.suby.designpattern.structural.decorator.example;

public class Execute {
    public static void main(String[] args) {
        Display road = new RoadDisplay();
        road.draw();    // 기본 도로 표기
        System.out.println();
        Display roadWithLane = new LaneDecorator(new RoadDisplay());
        roadWithLane.draw();    // 기본 도로 표시 + 차선표시
        System.out.println();
        Display roadWithTraffic = new TrafficDecorator(new RoadDisplay());
        roadWithTraffic.draw(); // 기본도로 표시 + 교통량 표시
        System.out.println();

        Display roadWithLaneWithTraffic = new TrafficDecorator(roadWithLane);
        roadWithLaneWithTraffic.draw(); // 기본도로 표시 + 차선표시 + 교통량 표시
    }
}
