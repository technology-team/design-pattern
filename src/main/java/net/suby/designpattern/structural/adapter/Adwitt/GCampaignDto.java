package net.suby.designpattern.structural.adapter.Adwitt;

import lombok.Builder;

@Builder
public class GCampaignDto {
    private String campaignName;
    private String budget;
}
