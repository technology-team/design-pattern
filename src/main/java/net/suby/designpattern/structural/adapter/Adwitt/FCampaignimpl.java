package net.suby.designpattern.structural.adapter.Adwitt;

public class FCampaignimpl implements FCampaign {
    @Override
    public void createCampaign(FCampaignDto fCampaignDto) {
        System.out.println("Facebook Create Campaign");
    }

    @Override
    public void createCampaign() {
        System.out.println("Facebook Create Campaign");
    }
}
