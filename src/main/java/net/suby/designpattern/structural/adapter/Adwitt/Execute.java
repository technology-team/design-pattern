package net.suby.designpattern.structural.adapter.Adwitt;

public class Execute {
    public static void main(String[] args) {
        FCampaignDto fCampaignDto = FCampaignDto.builder()
                .name("campaign name")
                .budget(10000)
                .build();

        FCampaign fCampaign = new FCampaignimpl();
        fCampaign.createCampaign(fCampaignDto);

        fCampaign = new FtoGAdapter(new GCampaignImpl());
        fCampaign.createCampaign(fCampaignDto);
    }
}
