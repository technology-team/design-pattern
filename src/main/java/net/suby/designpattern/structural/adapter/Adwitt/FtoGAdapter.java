package net.suby.designpattern.structural.adapter.Adwitt;

public class FtoGAdapter implements FCampaign {
    private GCampaign gCampaign;

    public FtoGAdapter(GCampaign gCampaign) {
        this.gCampaign = gCampaign;
    }

    @Override
    public void createCampaign(FCampaignDto fCampaignDto) {
        GCampaignDto gCampaignDto = GCampaignDto.builder()
                .campaignName(fCampaignDto.getName())
                .budget(String.valueOf(fCampaignDto.getBudget()))
                .build();
        this.gCampaign.makeCampaign(gCampaignDto);
    }

    @Override
    public void createCampaign() {
        this.gCampaign.makeCampaign();
    }
}
