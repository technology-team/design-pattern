package net.suby.designpattern.structural.adapter.example;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Execute {

    public static void main(String[] args) {
        MediaPlayer player = new MP3();
        player.play("file.mp3");

        //player = new MP4();

        player = new FormatAdater(new MP4());
        player.play("file.mp4");


        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    }
}
