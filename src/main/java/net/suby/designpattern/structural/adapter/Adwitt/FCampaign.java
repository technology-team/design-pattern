package net.suby.designpattern.structural.adapter.Adwitt;

public interface FCampaign {
    public void createCampaign(FCampaignDto fCampaignDto);

    public void createCampaign();
}
