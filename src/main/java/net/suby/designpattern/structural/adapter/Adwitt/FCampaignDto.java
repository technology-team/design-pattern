package net.suby.designpattern.structural.adapter.Adwitt;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class FCampaignDto {
    private String name;
    private int budget;
}
