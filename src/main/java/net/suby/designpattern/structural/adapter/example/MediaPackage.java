package net.suby.designpattern.structural.adapter.example;

public interface MediaPackage {
    void playFile(String fileName);
}
