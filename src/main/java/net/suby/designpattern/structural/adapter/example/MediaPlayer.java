package net.suby.designpattern.structural.adapter.example;

public interface MediaPlayer {
    void play(String fileName);
}
