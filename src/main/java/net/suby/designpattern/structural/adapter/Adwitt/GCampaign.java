package net.suby.designpattern.structural.adapter.Adwitt;

public interface GCampaign {
    public void makeCampaign(GCampaignDto gCampaignDto);

    public void makeCampaign();
}
