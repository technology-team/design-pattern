package net.suby.designpattern.structural.adapter.Adwitt;

public class GCampaignImpl implements GCampaign {
    @Override
    public void makeCampaign(GCampaignDto gCampaignDto) {
        System.out.println("Create Google Campaign");
    }

    @Override
    public void makeCampaign() {
        System.out.println("Create Google Campaign");
    }
}
