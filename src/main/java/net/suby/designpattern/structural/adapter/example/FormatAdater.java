package net.suby.designpattern.structural.adapter.example;

public class FormatAdater implements MediaPlayer {
    private MediaPackage media;

    public FormatAdater(MediaPackage media) {
        this.media = media;
    }

    @Override
    public void play(String fileName) {
        System.out.println("Using Adater");
        media.playFile(fileName);
    }
}
